<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "ROI@home");

Route::post("/results", "ROI@calcResults");

Route::post("/compare", "ROI@calcComparison");

Route::post("/recomp", "ROI@recompare");