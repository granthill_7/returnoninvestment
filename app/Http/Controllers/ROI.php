<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Investment;

class ROI extends Controller
{
    //
    function index(Request $request)
    {
        
    }
    
    function home()
    {
        #mysqli_query($connection, "DELETE FROM investments");
        Investment::truncate();
        return view('investment_app');
    }
    
    function calcResults(Request $req)
    {
        $investment = new Investment([
            'principal'     =>  $principal = $req->input('principal'),
            'annualReturn'  =>  $annualReturn = $req->input('annualReturn'),
            'taxRate'       =>  $taxRate = $req->input('taxRate'),
            'inflationRate' =>  $inflationRate = $req->input('inflationRate'),
            'bankChargeType'=>  $bankChargeType = $req->input('bankChargeType'),
            'bankCharge'    =>  $bankCharge = $req->input('bankCharge'),
            'years'         =>  $years = $req->input('years')
        ]);  
        $investment->save();   
        
        $investments = Investment::all()->toArray();
        return view('results', compact('investments'));
    }
    
    function calcComparison(Request $req)
    {
        $investment = new Investment([
            'principal'     =>  $principal = $req->input('principal'),
            'annualReturn'  =>  $annualReturn = $req->input('annualReturn'),
            'taxRate'       =>  $taxRate = $req->input('taxRate'),
            'inflationRate' =>  $inflationRate = $req->input('inflationRate'),
            'bankChargeType'=>  $bankChargeType = $req->input('bankChargeType'),
            'bankCharge'    =>  $bankCharge = $req->input('bankCharge'),
            'years'         =>  $years = $req->input('years')
        ]);
        $investment->save();
        
        $investments = Investment::all()->toArray();        
        return view('compare', compact('investments'));
    }
    
    function recompare()
    {
        $investments = Investment::all()->toArray();
        return view('compare', compact('investments'));
    }
    

}
