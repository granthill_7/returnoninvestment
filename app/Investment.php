<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investment extends Model
{
    protected $fillable = ['principal', 'annualReturn', 'taxRate', 'inflationRate',
                           'bankChargeType', 'bankCharge', 'years'];
    protected $primaryKey = 'entry';
}
