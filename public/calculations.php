<?php 

#---- Functions ----
//Function including tax and bank charge, but not inflation
function nominalAmountAvailable($prin, $annRt, $taxRt, $bnkChType, $bnkCh, $yrs)
{
    //rate after tax, before inflation
    $resultRate = ($annRt / 100) * (1 - ($taxRt / 100));
    
    $finalArray = calcBankCharge($resultRate, $prin, $annRt, $bnkChType, $bnkCh, $yrs);
    
    return $finalArray;
}

//Function including tax, inflation, and bank charge
function inflationAmountAvailable($prin, $annRt, $taxRt, $inflRate, $bnkChType, $bnkCh, $yrs)
{
    //rate after tax, before inflation
    $resultRate = ($annRt / 100) * (1 - ($taxRt / 100));
    
    //rate after tax and inflation
    $resultRate = ((1 + $resultRate) / (1 + ($inflRate / 100))) - 1;
    
    $finalArray = calcBankCharge($resultRate, $prin, $annRt, $bnkChType, $bnkCh, $yrs);
    
    return $finalArray;
}

//Function to subtract the bank charges; returns array of final values
function calcBankCharge($finalRate, $initial, $intRate, $chargeType, $charge, $time)
{
    //Create final array and initalize year 0
    $result = array();
    array_push($result, $initial);
    
    
    for($i = 1; $i <= $time; $i++)
    {
        $newVal = $result[$i - 1];
        $interestEarned = $newVal * ($intRate / 100);
        
        $bankCharge = 0;
        if(strcmp($chargeType, "Interest Earned") === 0)
        {
            $bankCharge = floatval($interestEarned) * (floatval($charge) / 100);
        }
        else
        {
            $bankCharge = floatval($initial) * (floatval($charge) / 100);
        }
        
        $valAfterTax = $newVal * (1 + $finalRate);
        
        $finalVal = $valAfterTax - $bankCharge;
        
        array_push($result, $finalVal);
    }
    
    return $result;
}

?>