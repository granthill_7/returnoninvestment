## Return On Investment Project

This program uses the default Laravel framework and includes Chart.js to create graphs.

1. Enter data into home page. Click the **Calculate** button to advance to Results page, or the **Clear** button to clear the text boxes.

2. On Results page, there are another set of text boxes. These are for comparing another set of values and line up with the label across from them

3. Click the **Compare** button to access the Compare page with the given values, the **Clear** button to clear the text boxes, or the **Home** button to go back to the home screen.

4. On the Compare page, use the **New Comparison** button to view a new comparison, or look below the graph for all previously entered entries that can be looked at again


---

Important usage notes:

1. In order to run the app in localhost, first configure the /config/database.php file in line 51 to point to the name of your MySQL database, and configure the .env file in the main directory so that line 12 points to that same database.

2. After pointing the previous two files to the correct DB, run "php artisan migrate" in the project folder to set up the DB for use.

3. Returning to the home screen clears out the database for a new user.
