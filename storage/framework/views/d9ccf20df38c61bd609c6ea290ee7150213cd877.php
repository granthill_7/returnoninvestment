<html>
<head>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/results.css')); ?>">
</head>
<body style="background-color:#FEFFA8;" >

<h1>Results</h1>

<?php
#initialize variables from POST and write functions for calculations

#---- Variables ----
$principal = $_POST["principal"];
$annualReturn = $_POST["annualReturn"];
$taxRate = $_POST["taxRate"];
$inflationRate = $_POST["inflationRate"];
$bankChargeType = $_POST["bankChargeType"];
$bankCharge = $_POST["bankCharge"];
$years = $_POST["years"];

include 'calculations.php';

?>

					
					
<div style="position:absolute; left:310px; top:46px">Compare values:</div>
<form action="/compare" method="post">
<?php echo csrf_field(); ?>

    <span>Principal: $<?php echo $principal;?> </span> 
    <input type="number" name="compPrincipal" required><br><br>
    
    <span>Annual Return: <?php echo $annualReturn;?>% </span> 
    <input type="number" name="compAnnualReturn" required><br><br>
    
    <span>Tax Rate: <?php echo $taxRate;?>% </span> 
    <input type="number" name="compTaxRate" required><br><br>
    
    <span>Inflation Rate: <?php echo $inflationRate;?>% </span> 
    <input type="number" name="compInflationRate" required><br><br>
    
    <span>Bank Charge Type: <?php echo $bankChargeType;?> </span> 
    <select name="compBankChargeType" style="margin-left:73px;">
    	<option value="Interest Earned">Interest Earned</option>
    	<option value="Invested Amount">Invested Amount</option>
    </select>
    <br><br>
    
    <span>Bank Charge: <?php echo $bankCharge;?>% </span> 
    <input type="number" name="compBankCharge" required><br><br>
    
    <span>Years: <?php echo $years;?> </span> 
    <input type="number" name="compYears" required><br><br>
    
    
    <input type="hidden" name="principal" value=<?php echo "$principal"?>>
    <input type="hidden" name="annualReturn" value=<?php echo "$annualReturn"?>>
    <input type="hidden" name="taxRate" value=<?php echo "$taxRate"?>>
    <input type="hidden" name="inflationRate" value=<?php echo "$inflationRate"?>>
    <input type="hidden" name="bankChargeType" value="<?php echo "$bankChargeType"?>">
    <input type="hidden" name="bankCharge" value=<?php echo "$bankCharge"?>>
    <input type="hidden" name="years" value=<?php echo "$years"?>>
    
    <input type="submit" value="Compare" style="position:absolute; left:600px; top:80px">
    <input type="reset" value="Clear" style="position:absolute; left:600px; top: 155px">
</form>
<form action="/" method="get">
    <input type="submit" value="Home" style="position:absolute; left:600px; top: 230px">
</form>

					
	
<?php 
#Create arrays with amounts for each year
$nominalAmounts = nominalAmountAvailable($principal, $annualReturn, $taxRate, $bankChargeType,
                                         $bankCharge, $years);

$inflationAmounts = inflationAmountAvailable($principal, $annualReturn, $taxRate, $inflationRate,
                                             $bankChargeType, $bankCharge, $years);
#initialize yearsArray
$yearsArray = array();
for($i = 0; $i <= $years; $i++)
{
    array_push($yearsArray, $i);
}

?>

<table class="center">
	<tr>
		<th>Year</th>
		<th style="color:#FE0000;">Amount Available<br>(nominal)</th>
		<th style="color:#001B9F;">Amount Available<br>(inflation-adjusted)</th>
	</tr>
	
	<?php 	
	#Dynamically allocate table
	for($j = 0; $j < $years; $j++)
	{
	    $currYear = $j + 1;
	    echo "<tr>";
	    echo "<td> " . $currYear . "</td>";
	    echo "<td> $" . round($nominalAmounts[$j], 2) . "</td>";
	    echo "<td> $" . round($inflationAmounts[$j], 2) . "</td>";
	    echo "</tr>";
	}
	?>
</table>

					

<div class="roiChartDiv">
    <canvas id="roiChart" height="450" width="650">
    </canvas><br>
</div>

<script type="text/javascript">

//Create roiChart and input data
var ctx = document.getElementById("roiChart");
var roiChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: <?php echo json_encode($yearsArray) ?>,
		datasets: [
			{
				data: <?php echo json_encode($nominalAmounts) ?>,
				label: "Nominal",
				borderColor: "#FE0000",
				backgroundColor: "#FE0000",
				fill: false
			},
			{
				data: <?php echo json_encode($inflationAmounts) ?>,
				label: "Inflation-Adjusted",
				borderColor: "#001B9F",
				backgroundColor: "#001B9F",
				fill: false
			}
		]
	}
});

</script>

</body>
</html>
<?php /**PATH C:\xampp\htdocs\ROI\ROI\resources\views/results.blade.php ENDPATH**/ ?>