<html>
<head>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/compare.css')); ?>">
</head>
<body style="background-color:#FEFFA8; text-align:center; font-family:Helvetica;" >

<h1>Results</h1>

<?php
#initialize variables from MySQL and include functions for calculations

#---- Variables (original and comparison) ----
use App\Investment;

$numEntries = 0;
foreach($investments as $row)
{
    $numEntries++;
}

#$originalQ = mysqli_query($connection, "SELECT * FROM investments WHERE entry = 1");
$original = Investment::find(1);
#$original = mysqli_fetch_array($originalQ);
$entryNum;

if(isset($_POST["entry"]))
{
    $entryNum = $_POST["entry"];
}
else 
{
    $entryNum = $numEntries;
}

#$compareQ = mysqli_query($connection, "SELECT * FROM investments WHERE entry = " . $entryNum);
#$compare = mysqli_fetch_array($compareQ);
$compare = Investment::find($entryNum);

$principal = $original["principal"]; #$_POST["principal"];
$annualReturn = $original["annualReturn"];
$taxRate = $original["taxRate"];
$inflationRate = $original["inflationRate"];
$bankChargeType = $original["bankChargeType"];
$bankCharge = $original["bankCharge"];
$years = $original["years"];

$compPrincipal = $compare["principal"];
$compAnnualReturn = $compare["annualReturn"];
$compTaxRate = $compare["taxRate"];
$compInflationRate = $compare["inflationRate"];
$compBankChargeType = $compare["bankChargeType"];
$compBankCharge = $compare["bankCharge"];
$compYears = $compare["years"];

include 'calculations.php'; // in /public
?>

					
						
<div class="row">

	<div class="column"  style="fontsize:110%">
		<span>Principal: $<?php echo $principal;?> </span><br><br>
		<span>Annual Return: <?php echo $annualReturn;?>% </span><br><br>
		<span>Tax Rate: <?php echo $taxRate;?>% </span><br><br>
		<span>Inflation Rate: <?php echo $inflationRate;?>% </span><br><br>
		<span>Bank Charge Type: <?php echo $bankChargeType;?> </span><br><br>
		<span>Bank Charge: <?php echo $bankCharge;?>% </span><br><br>
		<span>Years: <?php echo $years;?> </span><br><br>
	</div>
	
	<div class="column">
		<span>Principal: $<?php echo $compPrincipal;?></span><br><br>
		<span>Annual Return: <?php echo $compAnnualReturn;?>%</span><br><br>
		<span>Tax Rate: <?php echo $compTaxRate;?>%</span><br><br>
		<span>Inflation Rate: <?php echo $compInflationRate;?>%</span><br><br>
		<span>Bank Charge Type: <?php echo $compBankChargeType;?></span><br><br>
		<span>Bank Charge: <?php echo $compBankCharge;?>%</span><br><br>
		<span>Years: <?php echo $compYears;?></span><br><br>
	</div>
	
	<div class="column">
		<form action="/compare" method="post">
		<?php echo csrf_field(); ?>

        	
            <input type="number" name="principal" required><br><br>   
                <input type="number" name="annualReturn" required><br><br>
              	<input type="number" name="taxRate" required><br><br>
                <input type="number" name="inflationRate" required><br><br>
                <select name="bankChargeType">
                	<option value="Interest Earned">Interest Earned</option>
                	<option value="Invested Amount">Invested Amount</option>
                </select>
                <br><br>
                <input type="number" name="bankCharge" required><br><br>
                <input type="number" name="years" required><br><br>
        
        	<input type="submit" value="New Comparison">
        </form>
	</div>
</div>						

					
	
<?php 
#Create arrays with amounts for each year

$row = $original;
$years = $row['years'];
$nominalAmounts = nominalAmountAvailable($row['principal'], $row['annualReturn'], $row['taxRate'], 
                                         $row['bankChargeType'], $row['bankCharge'], $row['years']);
$inflationAmounts = inflationAmountAvailable($row['principal'], $row['annualReturn'], $row['taxRate'], $row['inflationRate'],
                                             $row['bankChargeType'], $row['bankCharge'], $row['years']);

$row = $compare;
$compYears = $row['years'];
$compNominalAmounts = nominalAmountAvailable($row['principal'], $row['annualReturn'], $row['taxRate'],
                                             $row['bankChargeType'], $row['bankCharge'], $row['years']);
$compInflationAmounts = inflationAmountAvailable($row['principal'], $row['annualReturn'], $row['taxRate'], $row['inflationRate'],
                                                 $row['bankChargeType'], $row['bankCharge'], $row['years']);

#Determine max number of years and initialize yearsArray
$yearsArray = array();
$mostYears = $years;
if($years < $compYears) { $mostYears = $compYears; }

for($i = 0; $i <= $mostYears; $i++)
{
    array_push($yearsArray, $i);
}
?>

<table class="center">
	<tr>
		<th>Year</th>
		<th style="color:#FE0000;">Amount Available<br>(original, nominal)</th>
		<th style="color:#001B9F;">Amount Available<br>(original, inflation-adjusted)</th>
		<th style="color:#B00FB6;">Amount Available<br>(compared, nominal)</th>
		<th style="color:#0FB621;">Amount Available<br>(compared, inflation-adjusted)</th>
	</tr>
	
	<?php 
	#Dynamically allocate table
	for($j = 0; $j <= $mostYears; $j++)
	{
	    $currYear = $j;
	    echo "<tr>";
	    echo "<td> " . $currYear . "</td>";
	    
	    #If j is outside of years input, place "-" in the table
	    if($j <= $years) 
	    { 
	       echo "<td> $" . round($nominalAmounts[$j], 2) . "</td>";
	       echo "<td> $" . round($inflationAmounts[$j], 2) . "</td>"; 
	    }
	    else
	    {
	        echo "<td> - </td>";
	        echo "<td> - </td>";
	    }
	    
	    if($j <= $compYears) 
	    { 
	       echo "<td> $" . round($compNominalAmounts[$j], 2) . "</td>";
	       echo "<td> $" . round($compInflationAmounts[$j], 2) . "</td"; 
	    }
	    else 
	    {
	        echo "<td> - </td>";
	        echo "<td> - </td>";
	    }
	    
	    echo "</tr>";
	}
	?>
</table>

					

<div class="roiChartDiv">
    <canvas id="roiChart" height="350" width="650">
    </canvas><br>
</div>

<script type="text/javascript">

//Create roiChart and input data
var ctx = document.getElementById("roiChart");
var roiChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: <?php echo json_encode($yearsArray) ?>,
		datasets: [
			{
				<?php $tempColor = substr(md5(rand()), 0, 6);?>
				data: <?php echo json_encode($nominalAmounts) ?>,
				label: "Nominal (original)",
				borderColor: "#<?php echo $tempColor;?>",
				backgroundColor: "#<?php echo $tempColor;?>",
				fill: false
			},
			{
				<?php $tempColor = substr(md5(rand()), 0, 6);?>
				data: <?php echo json_encode($inflationAmounts) ?>,
				label: "Inflation-Adjusted (original)",
				borderColor: "#<?php echo $tempColor;?>",
				backgroundColor: "#<?php echo $tempColor;?>",
				fill: false
			},
			{
				<?php $tempColor = substr(md5(rand()), 0, 6);?>
				data: <?php echo json_encode($compNominalAmounts) ?>,
				label: "Nominal (comparison)",
				borderColor: "#<?php echo $tempColor;?>",
				backgroundColor: "#<?php echo $tempColor;?>",
				fill: false
			},
			{
				<?php $tempColor = substr(md5(rand()), 0, 6);?>
				data: <?php echo json_encode($compInflationAmounts) ?>,
				label: "Inflation-Adjusted (comparison)",
				borderColor: "#<?php echo $tempColor;?>",
				backgroundColor: "#<?php echo $tempColor;?>",
				fill: false
			}
			
		]
	}
});

</script>

					
					
<h3>Compare old entry #:</h3> 
	<form action="/recomp" method="post">
		<?php echo csrf_field(); ?>
		<input type="number" name="entry" required>
		<input type="submit" class="recompare" value="Compare">
	</form>
					
<table class='center'>
	<tr>
		<th>Entry #</th>
		<th>Principal</th>
		<th>Annual Return</th>
		<th>Tax Rate</th>
		<th>Inflation Rate</th>
		<th>Bank Charge Type</th>
		<th>Bank Charge</th>
		<th>Years</th>
	</tr>
	
	<?php foreach($investments as $row) { 
	    echo "<tr>";
	    echo "<td> " . $row['entry'] . "</td>";
	    echo "<td> " . $row['principal'] . "</td>";
	    echo "<td> " . $row['annualReturn'] . "</td>";
	    echo "<td> " . $row['taxRate'] . "</td>";
	    echo "<td> " . $row['inflationRate'] . "</td>";
	    echo "<td> " . $row['bankChargeType'] . "</td>";
	    echo "<td> " . $row['bankCharge'] . "</td>";
	    echo "<td> " . $row['years'] . "</td>";
	    echo "</tr>";
	}
	?>
</table>

</body>
</html>
<?php /**PATH C:\xampp\htdocs\ROI\returnoninvestment\resources\views/compare.blade.php ENDPATH**/ ?>