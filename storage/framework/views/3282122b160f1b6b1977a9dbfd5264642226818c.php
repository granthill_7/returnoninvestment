<html>
<head>
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/investment_app.css')); ?>">
</head>


<body style="background-color:#FEFFA8; font-family:Helvetica;">
<h1 style="text-align:center;">Estimate your ROI</h1>



<form action="/results" method="post" style="text-align:center;">
<?php echo csrf_field(); ?>	

    <div>Principal Amount:</div>
    $<input type="number" name="principal" required>
    <br><br>
    
    <div>Annual Return as a % (e.g. 3.5):</div>
    <input type="number" name="annualReturn" required>%
    <br><br>
    
    <div>Tax Rate as a % (e.g. 3.5):</div>
    <input type="number" name="taxRate" required>%
    <br><br>
    
    <div>Annual Inflation Rate:</div>
    <input type="number" name="inflationRate" required>%
    <br><br>
    
    <div>Annual Bank Charge as a % of:</div>
    <select name="bankChargeType">
    	<option value="Interest Earned">Interest Earned</option>
    	<option value="Invested Amount">Invested Amount</option>
    </select>
    <br>
    <input type="number" name="bankCharge" required>%
    <br><br>
    
    <div>Number of Years:</div>
    <input type="number" name="years" required>
    <br><br>
    
    <input type="submit" value="Calculate">
    <input type="reset" value="Clear">

</form>
<br><br>
</body>
</html>
<?php /**PATH C:\xampp\htdocs\ROI\returnoninvestment\resources\views/investment_app.blade.php ENDPATH**/ ?>