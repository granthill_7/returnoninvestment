<html>
<head>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/compare.css')); ?>">
</head>
<body style="background-color:#FEFFA8;" >

<h1>Results</h1>

<?php
#initialize variables from POST and write functions for calculations

#---- Variables (original and comparison) ----
$principal = $_POST["principal"];
$annualReturn = $_POST["annualReturn"];
$taxRate = $_POST["taxRate"];
$inflationRate = $_POST["inflationRate"];
$bankChargeType = $_POST["bankChargeType"];
$bankCharge = $_POST["bankCharge"];
$years = $_POST["years"];

$compPrincipal = $_POST["compPrincipal"];
$compAnnualReturn = $_POST["compAnnualReturn"];
$compTaxRate = $_POST["compTaxRate"];
$compInflationRate = $_POST["compInflationRate"];
$compBankChargeType = $_POST["compBankChargeType"];
$compBankCharge = $_POST["compBankCharge"];
$compYears = $_POST["compYears"];

include 'calculations.php';

?>

					
						
<span>Principal: $<?php echo $principal;?> </span> 
<span class="compSpan" style="top:70px">Principal: $<?php echo $compPrincipal;?></span>
<br><br>

<span>Annual Return: <?php echo $annualReturn;?>% </span> 
<span class="compSpan" style="top:105px">Annual Return: <?php echo $compAnnualReturn;?>%</span>
<br><br>

<span>Tax Rate: <?php echo $taxRate;?>% </span> 
<span class="compSpan" style="top:140px">Tax Rate: <?php echo $compTaxRate;?>%</span>
<br><br>

<span>Inflation Rate: <?php echo $inflationRate;?>% </span> 
<span class="compSpan" style="top:175px">Inflation Rate: <?php echo $compInflationRate;?>%</span>
<br><br>

<span>Bank Charge Type: <?php echo $bankChargeType;?> </span> 
<span class="compSpan" style="top:210px">Bank Charge Type: <?php echo $compBankChargeType;?></span>
<br><br>

<span>Bank Charge: <?php echo $bankCharge;?>% </span> 
<span class="compSpan" style="top:245px">Bank Charge: <?php echo $compBankCharge;?>%</span>
<br><br>

<span>Years: <?php echo $years;?> </span> 
<span class="compSpan" style="top:280px">Years: <?php echo $compYears;?></span>
<br><br>

<form action="/results" method="post">
<?php echo csrf_field(); ?>
	
    <input type="hidden" name="principal" value=<?php echo "$principal"?>>
    <input type="hidden" name="annualReturn" value=<?php echo "$annualReturn"?>>
    <input type="hidden" name="taxRate" value=<?php echo "$taxRate"?>>
    <input type="hidden" name="inflationRate" value=<?php echo "$inflationRate"?>>
    <input type="hidden" name="bankChargeType" value="<?php echo "$bankChargeType"?>">
    <input type="hidden" name="bankCharge" value=<?php echo "$bankCharge"?>>
    <input type="hidden" name="years" value=<?php echo "$years"?>>

	<input type="submit" value="Reset Comparison" style="position:absolute; left:570px; top: 150px">
</form>

					
	
<?php 
#Create arrays with amounts for each year
$nominalAmounts = nominalAmountAvailable($principal, $annualReturn, $taxRate, $bankChargeType,
                                         $bankCharge, $years);

$inflationAmounts = inflationAmountAvailable($principal, $annualReturn, $taxRate, $inflationRate,
                                             $bankChargeType, $bankCharge, $years);

$compNominalAmounts = nominalAmountAvailable($compPrincipal, $compAnnualReturn, $compTaxRate, 
                                             $compBankChargeType, $compBankCharge, $compYears);

$compInflationAmounts = inflationAmountAvailable($compPrincipal, $compAnnualReturn, $compTaxRate,
                                                 $compInflationRate, $compBankChargeType, 
                                                 $compBankCharge, $compYears);

#Determine max number of years and initialize yearsArray
$yearsArray = array();
$mostYears = $years;
if($years < $compYears) { $mostYears = $compYears; }

for($i = 0; $i <= $mostYears; $i++)
{
    array_push($yearsArray, $i);
}
?>

<table class="center">
	<tr>
		<th>Year</th>
		<th style="color:#FE0000;">Amount Available<br>(original, nominal)</th>
		<th style="color:#001B9F;">Amount Available<br>(original, inflation-adjusted)</th>
		<th style="color:#B00FB6;">Amount Available<br>(compared, nominal)</th>
		<th style="color:#0FB621;">Amount Available<br>(compared, inflation-adjusted)</th>
	</tr>
	
	<?php 
	#Dynamically allocate table
	for($j = 0; $j <= $mostYears; $j++)
	{
	    $currYear = $j;
	    echo "<tr>";
	    echo "<td> " . $currYear . "</td>";
	    
	    #If j is outside of years input, place "-" in the table
	    if($j <= $years) 
	    { 
	       echo "<td> $" . round($nominalAmounts[$j], 2) . "</td>";
	       echo "<td> $" . round($inflationAmounts[$j], 2) . "</td>"; 
	    }
	    else
	    {
	        echo "<td> - </td>";
	        echo "<td> - </td>";
	    }
	    
	    if($j <= $compYears) 
	    { 
	       echo "<td> $" . round($compNominalAmounts[$j], 2) . "</td>";
	       echo "<td> $" . round($compInflationAmounts[$j], 2) . "</td"; 
	    }
	    else 
	    {
	        echo "<td> - </td>";
	        echo "<td> - </td>";
	    }
	    
	    echo "</tr>";
	}
	?>
</table>

					

<div class="roiChartDiv">
    <canvas id="roiChart" height="450" width="650">
    </canvas><br>
</div>

<script type="text/javascript">

//Create roiChart and input data
var ctx = document.getElementById("roiChart");
var roiChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: <?php echo json_encode($yearsArray) ?>,
		datasets: [
			{
				data: <?php echo json_encode($nominalAmounts) ?>,
				label: "Nominal (original)",
				borderColor: "#FE0000",
				backgroundColor: "#FE0000",
				fill: false
			},
			{
				data: <?php echo json_encode($inflationAmounts) ?>,
				label: "Inflation-Adjusted (original)",
				borderColor: "#001B9F",
				backgroundColor: "#001B9F",
				fill: false
			},
			{
				data: <?php echo json_encode($compNominalAmounts) ?>,
				label: "Nominal (comparison)",
				borderColor: "#B00FB6",
				backgroundColor: "#B00FB6",
				fill: false
			},
			{
				data: <?php echo json_encode($compInflationAmounts) ?>,
				label: "Inflation-Adjusted (comparison)",
				borderColor: "#0FB621",
				backgroundColor: "#0FB621",
				fill: false
			}
		]
	}
});

</script>

</body>
</html>
<?php /**PATH C:\xampp\htdocs\ROI\ROI\resources\views/compare.blade.php ENDPATH**/ ?>