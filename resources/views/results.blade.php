<html>
<head>
	<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/results.css') }}">
</head>
<body style="background-color:#FEFFA8; text-align:center;" >

<h1 style="font-family:Helvetica;">Results</h1>

<?php
#initialize variables from POST and include functions for calculations
use App\Investment;

#---- Variables ----

#$originalQ = mysqli_query($connection, "SELECT * FROM investments WHERE entry = 1");
#$original = mysqli_fetch_array($originalQ);
$original = Investment::find(1);

$principal = $original["principal"]; #$_POST["principal"];
$annualReturn = $original["annualReturn"];
$taxRate = $original["taxRate"];
$inflationRate = $original["inflationRate"];
$bankChargeType = $original["bankChargeType"];
$bankCharge = $original["bankCharge"];
$years = $original["years"];



include 'calculations.php'; // in /public

?>

					{{-- Verify Inputs --}}
					
<div class="row">

	<div class="column" style="font-size:110%">
		<span>Principal: $<?php echo $principal;?> </span><br><br> 
		<span>Annual Return: <?php echo $annualReturn;?>% </span> <br><br> 
		<span>Tax Rate: <?php echo $taxRate;?>% </span><br><br> 
		<span>Inflation Rate: <?php echo $inflationRate;?>% </span><br><br> 
		<span>Bank Charge Type: <?php echo $bankChargeType;?> </span><br><br> 
		<span>Bank Charge: <?php echo $bankCharge;?>% </span><br><br> 
		<span>Years: <?php echo $years;?> </span><br><br> 
	</div>
	
    <form action="/compare" method="post">
    
    	<div class="column">
            @csrf
                <input type="number" name="principal" required><br><br>   
                <input type="number" name="annualReturn" required><br><br>
              	<input type="number" name="taxRate" required><br><br>
                <input type="number" name="inflationRate" required><br><br>
                <select name="bankChargeType">
                	<option value="Interest Earned">Interest Earned</option>
                	<option value="Invested Amount">Invested Amount</option>
                </select>
                <br><br>
                <input type="number" name="bankCharge" required><br><br>
                <input type="number" name="years" required><br><br>
                <input type="hidden" name="entry" value="2">
                     
        </div>
        
        <div class="column">
        	<input type="submit" value="Compare"> <br><br>
            <input type="reset" value="Clear"> <br><br>
        </div>    
    </form> 
       
   	<div class="column">
        <form action="/" method="get">
            <input type="submit" value="Home">
        </form>
	</div>
	
</div>
					{{-- Table of results --}}

<table class="center">
	<tr>
		<th>Year</th>
		<th style="color:#FE0000;">Amount Available<br>(nominal)</th>
		<th style="color:#001B9F;">Amount Available<br>(inflation-adjusted)</th>
	</tr>
	
	<?php 	
	#Create arrays with amounts for each year
	$nominalAmounts = nominalAmountAvailable($principal, $annualReturn, $taxRate, $bankChargeType,
	$bankCharge, $years);
	
	$inflationAmounts = inflationAmountAvailable($principal, $annualReturn, $taxRate, $inflationRate,
	    $bankChargeType, $bankCharge, $years);
	
	#initialize yearsArray
	$yearsArray = array();
	for($i = 0; $i <= $years; $i++)
	{
	    array_push($yearsArray, $i);
	}
	
	#Dynamically allocate table
	for($j = 0; $j < $years; $j++)
	{
	    $currYear = $j + 1;
	    echo "<tr>";
	    echo "<td> " . $currYear . "</td>";
	    echo "<td> $" . round($nominalAmounts[$j], 2) . "</td>";
	    echo "<td> $" . round($inflationAmounts[$j], 2) . "</td>";
	    echo "</tr>";
	}
	?>
</table>

					{{-- Make Graph using Chart.js --}}

<div class="roiChartDiv">
    <canvas id="roiChart" height="450" width="650">
    </canvas><br>
</div>

<script type="text/javascript">

//Create roiChart and input data
var ctx = document.getElementById("roiChart");
var roiChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: <?php echo json_encode($yearsArray) ?>,
		datasets: [
			{
				data: <?php echo json_encode($nominalAmounts) ?>,
				label: "Nominal",
				borderColor: "#FE0000",
				backgroundColor: "#FE0000",
				fill: false
			},
			{
				data: <?php echo json_encode($inflationAmounts) ?>,
				label: "Inflation-Adjusted",
				borderColor: "#001B9F",
				backgroundColor: "#001B9F",
				fill: false
			}
		]
	}
});

</script>

</body>
</html>
